import React ,{useState, useEffect} from 'react';
import {Link, Redirect} from 'react-router-dom';
import './HeaderUser.css';
import Logo from '../../../image/logo.png'
import api from '../../Services/Api';


const HeaderUser = () => {

    return (
        <header>
            <nav className="container">
                <Link className= "logo" to="/"><img id="logoimg" src={ Logo } />ShoppINg</Link>
                <ul>
                    <li>
                        <Link to="/TodosProd">Produtos</Link>
                    </li>
                    <li>
                        <Link to="/PerfilUser">Perfil</Link>
                    </li>
                    <li>
                        <Link to="/Login">Sair</Link>
                    </li>
                </ul>
            </nav>
        </header>
    )
}
export default HeaderUser;