import React from 'react';
import {Link} from 'react-router-dom';
import './HeaderGuest.css';
import Logo from '../../../image/logo.png'


function HeaderGuest () {

    return (
        <header>
            <nav className="container">
                <Link className= "logo" to="/"><img id="logoimg" src={ Logo } />ShoppINg</Link>
                <ul>
                    <li>
                        <Link to="/TodosProd">Produtos</Link>
                    </li>
                    <li>
                        <Link to="/Login">Login</Link>
                    </li>
                    <li>
                        <Link to="/Register">Registro</Link>
                    </li>
                </ul>
            </nav>
        </header>
    )
}
export default HeaderGuest;
