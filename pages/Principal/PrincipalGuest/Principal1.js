import React from 'react';
import Poltrona from '../../../image/poltrona.jpg'
import Sofa from '../../../image/Sofa.jpg'
import Sobrenos from '../../../image/Sobrenos.png'
import Puff from '../../../image/puff.jpg'
import Estante from '../../../image/estante.jpg'
import Mesa from '../../../image/mesa.jpg'
import './Principal1.css'
const Principal1 = () =>{
    return(
        <main>
            <section id="sct1pcpl">
                <picture>
                    <source media="(min-width:800px)" srcSet={Sofa} />
                    <source media="(max-width:800px)" srcSet={Poltrona} />
                    <img src={ Sofa } alt="Imagem chamativa de sofá" />
                </picture> 
                <h1 id="titulo1">e-Commerce ShoppINg</h1>
            </section>
            <section id="sct2pcpl">
                <h2 className="titulo2">Produtos mais vendidos!</h2>
                <div className="prod_pcpl container">
                    <div className="prod">
                        <figure className="prod_solo">
                            <img src={Estante} alt="Foto do produto"/>
                            <figcaption>Aparador</figcaption>   
                        </figure>
                        <p>Recebeu 345 likes</p>
                        <p>Estante</p>
                        <p>Maravilhoso!</p>
                        <h5>R$ 400,00</h5>
                    </div>
                    <div className="prod">
                        <figure className="prod_solo">
                            <img src={Puff} alt="Foto do produto"/>
                            <figcaption>Poltrona</figcaption>   
                        </figure>
                        <p>Recebeu 345 likes</p>
                        <p>Sofá</p>
                        <p>Super confortável!</p>
                        <h5>R$ 500,00</h5>
                    </div>
                    <div className="prod">
                        <figure className="prod_solo">
                            <img src={Mesa} alt="Foto do produto"/>
                            <figcaption>Mesa de Centro</figcaption>   
                        </figure>
                        <p>Recebeu 345 likes</p>
                        <p>Mesa</p>
                        <p>Linda demais!</p>
                        <h5>R$ 350,00</h5>
                    </div>
                </div>
            </section>
            <section id="sct3pcpl">
                <h2 className="titulo2">Sobre nós</h2>
                <div className="sobre_pcpl container">
                    <div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer lacinia diam fringilla mauris tristique sodales.
                        Aliquam nec dolor augue. Sed sollicitudin commodo massa, at tincidunt eros venenatis vitae. Mauris mollis vitae augue quis laoreet.
                        Maecenas tempor lacinia libero, eu dignissim dolor condimentum in. Vestibulum dapibus tortor quis libero sodales, nec posuere velit iaculis.
                        Donec varius lorem neque, eu vestibulum lectus blandit et. Duis porttitor suscipit magna non viverra. Maecenas at lorem nulla.
                        Donec ac tortor sed est auctor euismod non sed dui.
                        </p>
                    </div>
                    <div>
                        <img src={Sobrenos} alt="Membros da empresa" />
                    </div>
                </div>
            </section>
        </main>
    );
}
export default Principal1
