import React from  'react';
import HeaderGuest from '../HeaderGuest/HeaderGuest'
import Principal1 from './Principal1';
import Footer from '../Footer/Footer'

const Principal = () => {
    return(
    <div>
        <HeaderGuest/>
        <Principal1/>
        <Footer/>
    </div>
    );
}
export default Principal