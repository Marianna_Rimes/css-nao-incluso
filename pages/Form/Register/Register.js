import React, { useState, useRef } from 'react';
import './Register.css';
import axios from 'axios';

const Register = (props) => { //Tentei com useRef mas estava dando Erro :( 
    
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [password2, setPassword2] = useState('');
    const [cep, setCep] = useState('');
    const [endereco, setEndereco] = useState('');
    const [numero, setNumero] = useState('');
    const [cidade, setCidade] = useState('');
    const [bairro, setBairro] = useState('');
    const [uf, setUF] = useState('');
    const [fixo, setFixo] = useState('');
    const [celular, setCelular] = useState('');
    

    
    const handleInputChange = e => {
        const value = e.target.value;

        if(e.target.name === "name"){
            setName(value);
        }
        else if(e.target.name === "email"){
            setEmail(value);
        }
        else if(e.target.name === "password"){
            setPassword(value);
        }
        else if(e.target.name === "cep"){
            setCep(value);
        }
        else if(e.target.name === "endereco"){
            setEndereco(value);
        }
        else if(e.target.name === "numero"){
            setNumero(value);
        }
        else if(e.target.name === "cidade"){
            setCidade(value);
        }
        else if(e.target.name === "bairro"){
            setBairro(value);
        }
        else if(e.target.name === "uf"){
            setUF(value);
        }
        else if(e.target.name === "fixo"){
            setFixo(value);
        }
        else if(e.target.name === "celular"){
            setCelular(value);
        }
        else{
            setPassword2(value);
        }
        
    }
     
    const handleInputCep = (e, setFieldValue) => {
        const { value } = e.target;
        const cep = value?.replace(/[^0-9]/g, '');
        if(cep?.length !== 8){
            return;
        }

        fetch(`https://viacep.com.br/ws/${value}/json`)
        .then((res) => res.json())
        .then((data) => console.log(data));

        //setFieldValue('endereco', data.endereco);
    }

    const handleSubmit = e => {
        e.preventDefault()
        
        const postBody = {
            "user": {
                "name": name,
                "email": email,
                "password": password,
                "password_confirmation": password2,
                "cep": cep,
               // "admin": 0,
                "endereco": endereco,
                "numero": numero,
                "cidade": cidade,
                "bairro": bairro,
                "uf": uf,
                "fixo": fixo,
                "celular": celular
            }
        }
        console.log(postBody)
        axios.post('https://projetofinalin.herokuapp.com/sign_up', postBody)
            .then( response => {
                console.log(response)
            })
    }

    return (
        <section id="sct_rgt">
            <form onSubmit={handleSubmit}>
                <h2>Fazer Cadastro</h2>
                <label>Seu nome:</label>
                <input type="text" name="name" placeholder="Ex.: João" onChange={handleInputChange}/>
                <label>Seu email:</label>
                <input type="email" name="email" placeholder="email@hotmail.com" onChange={handleInputChange}/>
                <div className="grid1">    
                    <label>Sua senha:</label>
                    <label>Confirme sua senha:</label>
                    <input type="password" name="password" placeholder="Senha" onChange={handleInputChange}/>
                    <input type="password" name="password2" placeholder="Repita a senha" onChange={handleInputChange}/>
                </div>
                <div className="grid2">
                    <label>CEP:</label>
                    <input type="text" name="cep" placeholder="00000-000" onChange={handleInputChange}/>
                    <button><a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank">Buscar</a></button>
                </div>
                <div className="flex">
                    <label>Endereço:</label>
                    <input type="text" name="endereco" onChange={handleInputChange}/>
                </div>
                <div className="grid3">
                    <label>Número:</label>
                    <label>Bairro:</label>
                    <input type="text" name="numero" onChange={handleInputChange}/>
                    <input type="text" name="bairro" onChange={handleInputChange}/>
                    <label>Cidade:</label>
                    <label>UF:</label>
                    <input type="text" name="cidade"  onChange={handleInputChange}/>
                    <input type="text" name="uf" onChange={handleInputChange}/>
                </div>
                <h4>Telefones</h4>
                <div className="grid4">    
                    <label>Fixo:</label>
                    <label>Celular:</label>
                    <input type="text" name="fixo" placeholder="(XX) 9999-9999" onChange={handleInputChange}/>
                    <input type="text" name="celular" placeholder="(XX) 9 9999-9999" onChange={handleInputChange}/>
                </div>
                <input type="submit" value="Registrar"/>
            </form>
        </section>
    );

}
export default Register;