import React from 'react';
import HeaderGuest from '../../Principal/HeaderGuest/HeaderGuest'
import Register from './Register';
import Footer from '../../Principal/Footer/Footer';


const RegisterPage = () => {
    return(
        <div>
        <HeaderGuest/>
        <Register/>
        <Footer/>
        </div>
    );
}
export default RegisterPage