import React from 'react';
import HeaderGuest from '../../Principal/HeaderGuest/HeaderGuest'
import Login from './Login';
import Footer from '../../Principal/Footer/Footer';


const LoginPage = () => {
    return(
        <div>
        <HeaderGuest/>
        <Login/>
        <Footer/>
        </div>
    );
}
export default LoginPage