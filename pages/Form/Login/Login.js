import React, { useState } from 'react';
import api from '../../Services/Api';
import history from '../../../history';
import './Login.css';
import { Redirect } from 'react-router-dom';

const Login = (props) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [redirect, setRedirect] = useState(false);
    const handleLogin = async (e) => {
      e.preventDefault();
        let loginbody = {
          "user":{
                "email": email,
                "password": password
          }
      }
     const response = await api.post('/login', loginbody)
     if(response.status === 200){
         console.log(response.status)
         setRedirect(true)
     }
     else{
        console.log(response.status)
     }
    }

    const handleInputChange = e => {
        const value = e.target.value

        switch (e.target.name) {
            case "email":
                setEmail(value)
                break
            case "password":
                setPassword(value)
                break
            default:
        }
    }
        if(redirect){
            return <Redirect to='/PrincipalUser'/>
        }
        return  (
            <section id="sct_lgn">
                <form onSubmit={handleLogin} onChange={handleInputChange}>
                    <h2 className='titulo2'>Fazer Login</h2>
                    <label>Nome</label>
                    <input type="email" name="loginname"/>
                    <label>Senha</label>
                    <input type="password" name="loginpassword"/>
                    <input type="submit" value="Entrar"/>
                </form>                               
            </section>
        )  
    }
export default Login