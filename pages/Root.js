import React ,{useState, useEffect} from  'react';
import { BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom';
import RegisterPage from './Form/Register/Registerpage'
import LoginPage from './Form/Login/Loginpage'
import Principal from './Principal/PrincipalGuest/PrincipalGuest'
import HeaderGuest from './Principal/HeaderGuest/HeaderGuest'
import AuthenticationServices from './Services/Auth';
import PrincipalGuest from './Principal/PrincipalGuest/PrincipalGuest';
import PrincipalUser from './Principal/PrincipalUser/PrincipalUser'
import TodosProd from './Produtos/TodosProd/TodosProd'


const Root = () => {
    const [isLogged, setIsLogged] =  useState(false);
    const [user, setUser] = useState({});
    useEffect(() =>{
        AuthenticationServices(setIsLogged, setUser)
    }, [])
    console.log(setIsLogged)
    return(
        <div>
        <Router>
            <Switch>
            <Route exact path="/">
            {isLogged? <Redirect to = "/PrincipalUser"/>:<PrincipalGuest/>}
            </Route>
            <Route exact path="/Login">
            {isLogged? <Redirect to = "/PrincipalUser"/>:<LoginPage/>}
            </Route>
            <Route exact path="/Register">
            {isLogged? <Redirect to = "/PrincipalUser"/>:<RegisterPage/>}
            </Route>
            <Route exact path="/TodosProd">
            <TodosProd/>
            </Route>
            <Route exact path="/PrincipalUser">
            <PrincipalUser/>
            </Route>
                {/* <Route path="/Principal" component={Principal}/>
                <Route path="/Login" component={Login}/>
                <Route path="/Register" component={Register}/>
                 <Route>{isLogged? <Redirect to = "/Principal"/>:<Login/>}</Route> */}
            </Switch>
        </Router>
        </div>
    );
}
export default Root;