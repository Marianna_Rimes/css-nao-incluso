import api from './Api';

const AuthenticationServices = (setIsLogged, setUser) =>{
        let packageToken = localStorage.getItem("token");
        if(packageToken !== null){
            let config = {headers:
                {
                    "Authorization": packageToken
                }
            }
            api.get("/current_user", config) // Pode ser mudado ATENÇÃO
            .then((resp) =>{
                setIsLogged(true);
                setUser(resp.data.user);

            })
            .catch(console.log)
        }
}
export default AuthenticationServices