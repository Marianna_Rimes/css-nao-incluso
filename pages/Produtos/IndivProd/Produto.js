import React, { useState }from 'react';
import { Redirect } from 'react-router-dom';
import './Produto.css'
const Produto = (props) => {
    const [product, setProduct] = useState(props.product)
    const [isLogged, setisLogged] = useState({});


    const perfilProduct = () => {
        // <Redirect to='/Productprofile'/>
    }

    return (
        <div className="prod">
            <figure className="prod_solo">
                {/* <img src={} alt="Foto do produto"/> */}
                <figcaption>{product.name }</figcaption>   
            </figure>
            <p>Recebeu {product.likes} likes</p>
            <p>{product.category}</p>
            <p>{product.description}</p>
            <h5>R$ {product.price}</h5>
            {isLogged ? <button onClick={perfilProduct}>Ver mais</button> :  <h3>Cadastre-se para ver mais!</h3>}
        </div>
    )
} 
export default Produto
