import React from  'react';
import HeaderGuest from '../../Principal/HeaderGuest/HeaderGuest'
import TodosProd from './TodosProd';
import Footer from '../../Principal/Footer/Footer'

const produtosPage = () => {
    return(
    <div>
        <HeaderGuest/>
        <TodosProd/>
        <Footer/>
    </div>
    );
}
export default produtosPage