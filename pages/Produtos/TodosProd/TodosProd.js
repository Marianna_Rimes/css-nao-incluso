import React, { useState, useEffect } from 'react';
import Produto from '../IndivProd/Produto';
import './TodosProd.css';
import axios from 'axios';

const TodosProd = props => {
    const [products, setProducts] = useState([]);
    const [searchName, setSearchName] = useState('');
    const [category, setCategory] = useState("Categorias");
    const [minValue, setMinValue] = useState('');
    const [maxValue, setMaxValue] = useState('');

    useEffect( () => {
        axios.get('https://projetofinalin.herokuapp.com/products')
        .then((response)=>{
            setProducts(response.data);
        })
    }, [])

    const handleInputChange = e => {
        switch (e.target.name) {
            case "search_name":
                setSearchName(e.target.value);
                break;
            case "category":
                setCategory(e.target.value);
                break;
            case "min_value":
                setMinValue(e.target.value);
                break;
            case "max_value":
                setMaxValue(e.target.value);
                break;
            default:
        }
    }

    const filterProduto = (Produto) => {
        let show = true

        if (!Produto.name.toUpperCase().includes(searchName.toUpperCase()) && searchName !== "") {
            console.log("filterName");
            show = false;
        } else if (category !== "Categorias" && Produto.category !== category) {
            console.log("filterCategory");
            show = false;
        } else if (minValue !== "" && minValue !== "0" && parseFloat(Produto.price) < minValue) {
            console.log("filterMin");
            show = false;
        } else if (maxValue !== "" && maxValue !== "0" && parseInt(Produto.price) > maxValue) {
            console.log("filterMax");
            show = false;
        }

        return show
    }

    return (
        <section id="sct_prod">
            <h3 id="loja">Loja</h3>
            <div className="container">
                <div>
                    <div className="flex">
                        <label className="label_prod">Digite um Produto:</label>
                        <input className="input_prod" type="text" placeholder="Ex.: Sofá" onChange={handleInputChange}/>
                    </div>
                    <div id="div2">
                        <div className="flex">
                            <label className="label_prod">Categoria:</label>
                            <select className="input_prod" onChange={handleInputChange} >
                                <option value="" disabled selected hidden>Selecionar</option>
                                <option value="Categorias">Sofás</option>
                                <option value="Celularares">Mesas</option>
                                <option value="Eletrodomésticos">Estantes</option>
                            </select> 
                        </div>
                        <div className="flex">
                            <label className="label_prod">Preço:</label>
                            <input className="input_prod" type="number" placeholder="min" onChange={handleInputChange}/>
                            <input className="input_prod" type="number" placeholder="máx" onChange={handleInputChange}/>
                        </div>
                    </div>
                </div>
                <div className='prods'>
                    {products.map( product => {
                        if (filterProduto(product)) {
                            return <Produto product={product} />
                        }
                    })}
                </div>
            </div>
        </section>
    )
}
export default TodosProd